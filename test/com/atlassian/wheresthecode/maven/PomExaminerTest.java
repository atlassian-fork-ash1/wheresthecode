package com.atlassian.wheresthecode.maven;

import com.atlassian.fugue.Option;
import org.junit.Test;

import java.io.InputStream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

public class PomExaminerTest {

    @Test
    public void test_basic_exmination() throws Exception {

        InputStream is = getStreamFor("poms/pom.xml");

        Option<MavenProject> mavenProjects = new PomExaminer().extractProject(is);

        assertThat(mavenProjects.isDefined(), equalTo(true));
        MavenProject mavenProject = mavenProjects.get();
        assertThat(mavenProject.getGav().getArtifactId(), equalTo("servicedesk-parent-pom"));

        assertThat(mavenProject.getScmInfo().isDefined(), equalTo(true));
        MavenScmInfo mavenScmInfo = mavenProject.getScmInfo().get();
        assertThat(mavenScmInfo.getConnnection().get(), equalTo("scm:git:ssh://git@stash.atlassian.com:7997/JIRA/servicedesk.git"));
        assertThat(mavenScmInfo.getUrl().get(), equalTo("https://stash.atlassian.com/projects/JIRA/repos/servicedesk/"));

    }

    @Test
    public void test_no_scm() throws Exception {

        InputStream is = getStreamFor("poms/pom-no-scm.xml");

        Option<MavenProject> mavenProjects = new PomExaminer().extractProject(is);

        assertThat(mavenProjects.isDefined(), equalTo(true));
        MavenProject mavenProject = mavenProjects.get();
        assertThat(mavenProject.getScmInfo().isDefined(), equalTo(false));

    }

    private InputStream getStreamFor(String s) {
        return getClass().getClassLoader().getResourceAsStream(s);
    }
}