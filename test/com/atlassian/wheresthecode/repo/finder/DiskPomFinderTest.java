package com.atlassian.wheresthecode.repo.finder;

import com.atlassian.wheresthecode.maven.MavenGav;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class DiskPomFinderTest {

    private DiskPomFinder diskPomFinder;

    @Before
    public void setUp() throws Exception {
        diskPomFinder = new DiskPomFinder();
    }

    @Test
    public void testGavRetrieval() throws Exception {
        FindResult pomDotXml = diskPomFinder.findPomDotXml(new MavenGav("com.atlassian.jira", "atlassian-jira", "4.4"));
        assertThat(pomDotXml.isDefined(), equalTo(true));
        assertThat(pomDotXml.getPomDotXml(), notNullValue());


    }
}