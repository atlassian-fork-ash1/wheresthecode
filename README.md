# Wheres The Code? #

An IDEA plugin that allows quick access to the underlying code for library code.


### How do I get set up? ###

* Checkout the code
* Open the directory in IDEA
* Make sure you have the IDEA plugin SDK setup inside IDEA

### Who do I talk to? ###

* Brad Baker started this as part of 20% time and the occasional trip home on the train.  