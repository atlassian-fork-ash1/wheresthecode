package com.atlassian.wheresthecode.handlebars;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.github.jknack.handlebars.Template;

import java.io.IOException;

public final class Templates
{
    private static final Handlebars handlebars = new Handlebars();

    static {
        handlebars.registerHelper("startWithUpperCase", new Helper<String>() {
            @Override
            public CharSequence apply(String context, Options options) throws IOException {
                return startWithUpperCase(context);
            }
        });
        handlebars.registerHelper("singular", new Helper<String>() {
            @Override
            public CharSequence apply(String context, Options options) throws IOException {
                return singular(context);
            }
        });
        handlebars.registerHelper("upperSingular", new Helper<String>() {
            @Override
            public CharSequence apply(String context, Options options) throws IOException {
                return startWithUpperCase(singular(context));
            }
        });
        handlebars.registerHelper("optionType", new Helper<String>() {
            @Override
            public CharSequence apply(String context, Options options) throws IOException {
                return "Option<" + context + ">";
            }
        });
        handlebars.registerHelper("createOption", new Helper<String>() {
            @Override
            public CharSequence apply(String context, Options options) throws IOException {
                return "Option.option(" + context + ")";
            }
        });
    }

    private static String singular(String text) {
        return text.replaceAll("ies$", "y").replaceAll("s$", "");
    }

    private static String startWithUpperCase(String text) {
        return text.substring(0, 1).toUpperCase() + text.substring(1);
    }

    public static String eval(String templateName, Object context)
    {
        try {
            Template template = handlebars.compile("/templates/" + templateName);
            return template.apply(context);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
