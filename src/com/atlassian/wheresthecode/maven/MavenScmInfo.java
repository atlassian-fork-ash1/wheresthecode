package com.atlassian.wheresthecode.maven;

import com.atlassian.fugue.Option;

/**
 * Created by bbaker on 4/12/2015.
 */
public class MavenScmInfo {
    private final Option<String> connnection;
    private final Option<String> url;

    public MavenScmInfo(Option<String> connnection, Option<String> url) {

        this.connnection = connnection;
        this.url = url;
    }

    public Option<String> getConnnection() {
        return connnection;
    }

    public Option<String> getUrl() {
        return url;
    }
}
