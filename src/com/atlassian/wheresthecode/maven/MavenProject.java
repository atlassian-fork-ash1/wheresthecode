package com.atlassian.wheresthecode.maven;

import com.atlassian.fugue.Option;

public class MavenProject {
    MavenGav gav;
    Option<MavenGav> parentGav;
    Option<MavenScmInfo> scmInfo;

    public MavenGav getGav() {
        return gav;
    }

    public Option<MavenGav> getParentGav() {
        return parentGav;
    }

    public Option<MavenScmInfo> getScmInfo() {
        return scmInfo;
    }

    public MavenProject(MavenGav gav, Option<MavenGav> parentGav, Option<MavenScmInfo> scmInfo) {


        this.gav = gav;
        this.parentGav = parentGav;
        this.scmInfo = scmInfo;
    }

}
