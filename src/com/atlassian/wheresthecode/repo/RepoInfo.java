package com.atlassian.wheresthecode.repo;

import javax.annotation.Nullable;

/**
 * used in handlebars and hence nullable
 */
public class RepoInfo {

    public final Gav gav;
    public final RepoType type;
    public final String cloneCommand;
    public final String address;
    @Nullable
    public final String url;

    public RepoInfo(RepoType type, Gav gav, String cloneCommand, String address, @Nullable String  url) {
        this.gav = gav;
        this.cloneCommand = cloneCommand;
        this.type = type;
        this.address = address;
        this.url = url;
    }

    public String getCloneCommand() {
        return cloneCommand;
    }

    public RepoType getType() {
        return type;
    }

    public String getAddress() {
        return address;
    }

    @Nullable
    public String getUrl() {
        return url;
    }

    public Gav getGav() {
        return gav;
    }

    public enum RepoType {
        GIT, SVN, CVS, HG, OTHER, MISSING;
    }

    public static class Gav {
        public final String groupId;
        public final String artifactId;
        public final String version;

        public Gav(String groupId, String artifactId, String version) {
            this.groupId = groupId;
            this.artifactId = artifactId;
            this.version = version;
        }

        public String getGroupId() {
            return groupId;
        }

        public String getArtifactId() {
            return artifactId;
        }

        public String getVersion() {
            return version;
        }
    }
}
