package com.atlassian.wheresthecode.actions;

import com.atlassian.fugue.Option;
import com.atlassian.wheresthecode.repo.RepoFinder;
import com.atlassian.wheresthecode.repo.RepoInfo;
import com.atlassian.wheresthecode.ui.BalloonMsg;
import com.atlassian.wheresthecode.ui.CodeInfoWindow;
import com.intellij.execution.filters.TextConsoleBuilderFactory;
import com.intellij.execution.ui.ConsoleView;
import com.intellij.execution.ui.ConsoleViewContentType;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.vfs.VirtualFile;

/**
 * Common place to kick of the finding
 */
public class CommonFinderAction {

    private final RepoFinder repoFinder;

    public CommonFinderAction() {
        repoFinder = new RepoFinder();
    }

    public void update(AnActionEvent e) {
        Project project = e.getData(CommonDataKeys.PROJECT);
        if (project != null && !project.isDefault()) {
            Editor editor = e.getData(CommonDataKeys.EDITOR);
            VirtualFile file = e.getData(CommonDataKeys.VIRTUAL_FILE);
            VirtualFile[] files = e.getData(CommonDataKeys.VIRTUAL_FILE_ARRAY);
            if ((editor != null || file != null || files != null) && (editor == null || editor.getDocument().getTextLength() != 0)) {
                setState(e, true, true);
            } else {
                setState(e, false, false);
            }
        } else {
            setState(e, false, false);
        }
    }

    private void setState(AnActionEvent e, boolean enabled, boolean visible) {
        e.getPresentation().setVisible(visible);
        e.getPresentation().setEnabled(enabled);
    }

    public void actionPerformed(final AnActionEvent e) {
        try {
            Project project = e.getProject();

            Option<VirtualFile> file = Option.option(e.getData(CommonDataKeys.VIRTUAL_FILE));
            if (file.isDefined()) {
                Option<RepoInfo> repo = repoFinder.findRepo(project, file.get());
                if (repo.isDefined()) {
                    processRepoInfo(e.getProject(), repo.get());
                } else {
                    BalloonMsg.showBalloonPopup(e, MessageType.WARNING, "Unable to find code information");
                }
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }


    }

    private void processRepoInfo(Project project, RepoInfo repoInfo) {


        ConsoleView console = TextConsoleBuilderFactory.getInstance().createBuilder(project).getConsole();
        console.print("Found git info", ConsoleViewContentType.NORMAL_OUTPUT);

        new CodeInfoWindow().dialog(project, repoInfo).show();
    }

}
