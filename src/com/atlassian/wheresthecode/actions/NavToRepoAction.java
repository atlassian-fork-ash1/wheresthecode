package com.atlassian.wheresthecode.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;

public final class NavToRepoAction extends AnAction {

    private CommonFinderAction commonFinderAction = new CommonFinderAction();

    @Override
    public void actionPerformed(final AnActionEvent event) {
        commonFinderAction.actionPerformed(event);
    }

    @Override
    public void update(AnActionEvent e) {
        commonFinderAction.update(e);
    }
}
