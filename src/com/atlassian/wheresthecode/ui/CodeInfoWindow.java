package com.atlassian.wheresthecode.ui;

import com.atlassian.wheresthecode.handlebars.Templates;
import com.atlassian.wheresthecode.repo.RepoInfo;
import com.github.jknack.handlebars.Context;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.ui.components.JBScrollPane;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import java.awt.*;
import java.net.URL;

public class CodeInfoWindow {

    public DialogWrapper dialog(Project project, final RepoInfo repoInfo) {
        return new DialogWrapper(project) {
            @Override
            protected JComponent createCenterPanel() {
                return getHtmlComponent(repoInfo);
            }

            public void show() {
                setTitle("Code Repository Information");
                setModal(true);
                init();
                super.show();
            }

        };
    }

    public JComponent getHtmlComponent(RepoInfo repoInfo) {
        // create jeditorpane
        JEditorPane jEditorPane = new JEditorPane();

        // make it read-only
        jEditorPane.setEditable(false);

        // create a scrollpane; modify its attributes as desired
        JScrollPane scrollPane = new JBScrollPane(jEditorPane);

        // add an html editor kit
        HTMLEditorKit kit = new HTMLEditorKit();
        jEditorPane.setEditorKit(kit);

        jEditorPane.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    browseUrl(e.getURL());
                }
            }
        });


        // add some styles to the html
        buildStyles(kit);

        // create some simple html as a string
        String htmlString = buildHtml(repoInfo);

        // create a document, set it on the jeditorpane, then add the html
        Document doc = kit.createDefaultDocument();
        jEditorPane.setDocument(doc);
        jEditorPane.setText(htmlString);
        return jEditorPane;
    }

    private void browseUrl(URL url) {
        try {
            Desktop.getDesktop().browse(url.toURI());
        } catch (Exception ignored) {

        }
    }


    private void buildStyles(HTMLEditorKit kit) {
        StyleSheet styleSheet = kit.getStyleSheet();
//        styleSheet.addRule("body {background-color: #000000; color:#fffff; font-family:times; margin: 5px;}");
        styleSheet.addRule("body {margin: 5px;}");
        styleSheet.addRule("pre {font : 10px monaco; }");
        styleSheet.addRule("a {font : 10px monaco; }");
    }

    @NotNull
    private String buildHtml(RepoInfo repoInfo) {
        Context context = Context
                .newBuilder(repoInfo)
                //.combine("user", user)
                .build();
        return Templates.eval("repoInfo", context);
    }
}
